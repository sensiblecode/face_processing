# face_processing

Experiments with the ReKognition face processing API

[https://rekognition.com/developer/docs](https://rekognition.com/developer/docs)

```
Usage:  ReKognition.py -h
        ReKognition.py
        ReKognition.py -a
        ReKognition.py <name> <url> [-o <ofilename> -c <cfilename> -d <dfilename> -i <imagename> -a]
        ReKognition.py -l <filename> [-o <ofilename> -c <cfilename> -d <dfilename> -a]
        ReKognition.py -t <box_key> -f <n> [-o <ofilename> -c <cfilename> -d <dfilename> -a]
        ReKognition.py -r <dfilename> 

Options:
    -h,--help                                   show this help message
    -l <filename>,--load <filename>             load a picture dictionary from a json file
    -t <box_key>,--twitter <box_key>            enrich twitter data defined by a box/public_key string 
    -f <n>,--followers_count <n>                Limit the call to twitter to COUNT items
    -o <ofilename>,--outputname <ofilename>     Output sqlite filename
    -d <dfilename>,--dumpname <dfilename>       Output raw dump filename
    -c <cfilename>,--csvname <cfilename>        Output csv filename
    -r <dfilename>,--replay <dfilename>         Replays previously dumped raw data
    -a,--apicallsremaining                      API calls remaining
    -i,--imagediagnostic                        Outputs an image with boxes around faces found
```
 
Requires a separate `secrets.py` file containing `API_SECRET` and `API_KEY` obtained from ReKognition.com

`face_processing` using the Python Imaging Library (PIL) this requires you to install and link some libraries before use:

```
sudo apt-get install python-dev libjpeg-dev libfreetype6-dev zlib1g-dev
```

```
sudo ln -s /usr/lib/`uname -i`-linux-gnu/libfreetype.so /usr/lib/
sudo ln -s /usr/lib/`uname -i`-linux-gnu/libjpeg.so /usr/lib/
sudo ln -s /usr/lib/`uname -i`-linux-gnu/libz.so /usr/lib/
```