#!/usr/bin/env python
# encoding: utf-8

# TODO
# Implement reload and export of saved raw data
# Do some visualisation of twitter data?

""" ReKognition.py

Usage:  ReKognition.py -h
        ReKognition.py
        ReKognition.py -a
        ReKognition.py <name> <url> [-o <ofilename> -c <cfilename> -d <dfilename> -i <imagename> -a]
        ReKognition.py -l <filename> [-o <ofilename> -c <cfilename> -d <dfilename> -a]
        ReKognition.py -t <box_key> -f <n> [-o <ofilename> -c <cfilename> -d <dfilename> -a]
        ReKognition.py -r <dfilename> 

Options:
    -h,--help                                   show this help message
    -l <filename>,--load <filename>             load a picture dictionary from a json file
    -t <box_key>,--twitter <box_key>            enrich twitter data defined by a box/public_key string 
    -f <n>,--followers_count <n>                Limit the call to twitter to COUNT items
    -o <ofilename>,--outputname <ofilename>     Output sqlite filename
    -d <dfilename>,--dumpname <dfilename>       Output raw dump filename
    -c <cfilename>,--csvname <cfilename>        Output csv filename
    -r <dfilename>,--replay <dfilename>         Replays previously dumped raw data
    -a,--apicallsremaining                      API calls remaining
    -i,--imagediagnostic                        Outputs an image with boxes around faces found
"""


from __future__ import unicode_literals
import requests
import json
import operator
from docopt import docopt
import sys
import urllib
import scraperwiki
import dateutil.parser
import requests_cache
import csv
import codecs

import urllib2
from cStringIO import StringIO

from PIL import ImageDraw, Image
from dshelpers import batch_processor
from collections import OrderedDict
from secrets import API_KEY, API_SECRET

SUMMARY_TEMPLATE = OrderedDict({u'name': '-',
                                u'age': '-',
                                u'gender': '-',
                                u'race': '-',
                                u'emotion': '-',
                                u'has_glasses': '-',
                                u'target_number': '0',
                                u'total_targets': '0'})
jobs = 'face_aggressive_part_part_detail_gender_emotion_race_age_glass_mouth_open_wide_eye_closed'


base_url = "http://rekognition.com/func/api/?api_key=%s&api_secret=%s&jobs=%s&urls=%s"
test_url = "http://farm3.static.flickr.com/2566/3896283279_0209be7a67.jpg"
genders = ['female', 'male']
glasses = ['no', 'yes']

requests_cache.install_cache('ReKognition_cache')


def pp(o):
    print json.dumps(o, indent=1)


def get_twitter_data_by_api(box_key, limit):
    api_stub = "https://ds-ec2.scraperwiki.com/"
    query = urllib.quote_plus(
        'select * from twitter_followers limit ' + str(limit))
    query_string = api_stub + box_key + "/sql/?q=" + query
    r = requests.get(query_string)
    twitter_data = r.json()
    return twitter_data


def get_pic_list_from_twitter_data(profile):
    pic_list = {}
    mangled_url = profile['profile_image'].replace("_normal", "")
    pic_list[profile['name']] = mangled_url
    return pic_list


def enrich_twitter_db(box_key, limit):
    twitter_data = get_twitter_data_by_api(box_key, limit)
    total = len(twitter_data)
    raw_output = []
    summary_output = []
    with batch_processor(save_rows, batch_size=5000) as b:
        for i, profile in enumerate(twitter_data):
            print("\rProcessing {} of {}".format(i + 1, total))
            pic_list = get_pic_list_from_twitter_data(profile)
            raw = get_ReKognition_output(pic_list)
            summary = get_summary_from_raw(raw)
            rows = make_db_row(profile, summary)
            raw_output.append(raw)
            summary_output.extend(summary)
            for row in rows:
                b.push(row)
    print "Pushing to db..."
    return summary_output, raw_output


def save_rows(rows):
    scraperwiki.sqlite.save(
        unique_keys=['id', 'target_number'],
        data=rows,
        table_name="twitter_followers")


def make_db_row(profile, summary_output):
    # assert len(twitter_data) == len(summary_output)

    data_output = []
    for summary in summary_output:
        data = OrderedDict()
        data['id'] = profile['id']
        data['name'] = profile['name']
        data['screen_name'] = profile['screen_name']
        data['profile_url'] = profile['profile_url']
        data['profile_image'] = profile['profile_image']

        # Added data
        data['age'] = summary['age']
        data['gender'] = summary['gender']
        data['race'] = summary['race']
        data['emotion'] = summary['emotion']
        data['has_glasses'] = summary['has_glasses']
        data['target_number'] = int(summary['target_number'])
        data['total_targets'] = int(summary['total_targets'])

        data['description'] = profile['description']
        data['location'] = profile['location']
        data['url'] = profile['url']

        data['followers_count'] = profile['followers_count']
        data['following_count'] = profile['following_count']
        data['statuses_count'] = profile['statuses_count']

        data['created_at'] = dateutil.parser.parse(profile['created_at'])

        data['batch'] = profile['batch']

        data_output.append(data)

    return data_output


def get_ReKognition_output(pic_list):
    raw_output = []
    for name, pic_url in pic_list.iteritems():
        pic_info = get_face_info(pic_url)
        pic_data = {name: pic_info}
        raw_output.append(pic_data)

    return raw_output


def get_summary_from_raw(raw_output):
    summary_output = []
    for pic_data in raw_output:
        if pic_data[pic_data.keys()[0]].has_key('face_detection'):
            summary = summarise_face_info(pic_data)
        else:
            # faulty
            tmp = SUMMARY_TEMPLATE.copy()
            tmp['name'] = pic_data.keys()[0]
            summary = [tmp]

        summary_output.extend(summary)

    return summary_output


def get_face_info(pic_url):
    url = base_url % (API_KEY, API_SECRET, jobs, pic_url)
    r = requests.get(url)
    content = r.json()
    # if r.from_cache:
    #    print "ReKognition data pulled from cache"
    return content


def summarise_face_info(pic_data):
    summary_output = []
    name = pic_data.keys()[0]
    content = pic_data[name]
    n_targets = len(content['face_detection'])

    if n_targets == 0:
        name = pic_data.keys()[0]
        #print("no face found for {}".format(name))
        summary = SUMMARY_TEMPLATE.copy()
        summary['name'] = pic_data.keys()[0]
        summary_output.append(summary)
        return summary_output

    for i, target in enumerate(content['face_detection']):
        #print("Face found for {}".format(pic_data.keys()[0]))
        summary = SUMMARY_TEMPLATE.copy()

        gender = genders[int(round(target['sex']))]
        has_glasses = glasses[int(round(target['glasses']))]
        emotion = max(target['emotion'].iteritems(),
                      key=operator.itemgetter(1))[0]
        race = max(target['race'].iteritems(),
                   key=operator.itemgetter(1))[0]
        age = target['age']

        summary['name'] = name
        summary['age'] = age
        summary['gender'] = gender
        summary['race'] = race
        summary['emotion'] = emotion
        summary['has_glasses'] = has_glasses
        summary['target_number'] = i + 1
        summary['total_targets'] = n_targets
        summary_output.append(summary)

    return summary_output


def load_raw_output(filename=''):
    # Read one file
    # Or read all of the files
    # Or check the files for a particular target URL

    if filename == '':
        filename = 'output/44454831-0aa7-4c84-be81-2874f4b3c3bc.json'
    with open(filename, 'rb') as f:
        raw_output = json.load(f)

    return raw_output


def save_raw_output(raw_output, filename):
    with open('output/' + filename + '.json', 'wb') as outfile:
        json.dump(raw_output, outfile)
    return


def print_summary(summary_output):
    for summary in summary_output:
        print("\nFace summary for {}".format(summary['name']))
        print("Age:    \t{}".format(summary['age']))
        print("Gender: \t{}".format(summary['gender']))
        print("Emotion:\t{}".format(summary['emotion']))
        print("Race:   \t{}".format(summary['race']))
        print("Glasses?:\t{}".format(summary['has_glasses']))
        print("Target:  \t{} of {}".format(summary['target_number'],
                                           summary['total_targets']))
    return


def write_summary_to_csv(summary_output, filename):
    fields = ["name", "age", "gender", "race", "emotion",
              "has_glasses", "target_number", "total_targets"]
    with open(filename, 'wb') as f:
        dw = csv.DictWriter(f, fieldnames=fields)
        dw.writeheader()
    # continue on to write data
        for summary in summary_output:
            dw.writerow(summary)
    return


def get_api_calls_remaining():
    with requests_cache.disabled():
        url = base_url % (API_KEY, API_SECRET, jobs, test_url)
        r = requests.get(url)
        content = r.json()
        calls_remaining = int(content['usage']['quota'])
    return calls_remaining


def plain_summary(pic_list):
    raw_output = []
    summary_output = []

    # Get pic_data array (raw outpout)
    raw_output = get_ReKognition_output(pic_list)
    # get summary from pic_data array
    summary_output = get_summary_from_raw(raw_output)
    print_summary(summary_output)

    return summary_output, raw_output


def write_diagnostic_images(raw_output):
    for entry in raw_output:
        name = entry.keys()[0]
        url = entry[name]['url']
        image = read_image_from_url(url)
        draw = ImageDraw.Draw(image)
        for i, face in enumerate(entry[entry.keys()[0]]['face_detection']):
            xorg = face['boundingbox']['tl']['x']
            yorg = face['boundingbox']['tl']['y']
            width = face['boundingbox']['size']['width']
            height = face['boundingbox']['size']['height']
            draw.rectangle([(xorg, yorg), (xorg + width, yorg + height)])
            draw.text((xorg + width / 2, yorg + height / 2), str(i + 1))

        image.save("images/" + name + ".jpg")


def read_image_from_url(url):
    url_parts = url.split('//')
    schema = url_parts[0] + '//'
    url_stub = url_parts[1]
    encoded_url = urllib.quote(url_stub)
    file = StringIO(urllib2.urlopen(schema + encoded_url).read())
    img = Image.open(file)
    return img


def main(args):
    # If we're called with no arguments at all
    if len(sys.argv) == 1:
        with open("default_pic_list.json") as f:
            default_pic_list = json.load(f)
        summary_output, raw_output = plain_summary(default_pic_list)
        # print "default_pic_list"
    # if we are called with -t and -f set
    elif (args['--followers_count'] is not None 
              and args['--twitter'] is not None):
        # make scraperwiki format sqlite db
        # TODO get box_key and limit
        box_key = args['--twitter']
        limit = args['--followers_count']
        summary_output, raw_output = enrich_twitter_db(box_key, limit)
        # print("enrich twitter with {}, followers = {}".format(box_key, limit))
    elif (args['<name>'] is not None and args['<url>'] is not None):
        name = args['<name>']
        image_url = args['<url>']
        pic_dict = {name: image_url}
        summary_output, raw_output = plain_summary(pic_dict)
        # print("Calling ReKognition with {}, {}".format(name, image_url))
    elif args['--load'] is not None:
        with open(args['--load']) as f:
            default_pic_list = json.load(f)
        summary_output, raw_output = plain_summary(default_pic_list)

    # Do save to csv or dump here
    if args['--csvname'] is not None:
        write_summary_to_csv(summary_output, args['--csvname'])
    if args['--dumpname'] is not None:
        save_raw_output(raw_output, args['--dumpname'])

    # Do image diagnostic here
    if args['--imagediagnostic']:
        write_diagnostic_images(raw_output)
    # print api calls remaining
    if args['--apicallsremaining']:
        print("\nAPI calls remaining: \t {}".format(get_api_calls_remaining()))

if __name__ == "__main__":
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    args = docopt(__doc__)
    main(args)
