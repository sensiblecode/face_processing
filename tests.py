#!/usr/bin/env python
# encoding: utf-8

from __future__ import unicode_literals
import unittest
import json

from nose.tools import assert_equal
from collections import OrderedDict

from ReKognition import (load_raw_output, summarise_face_info, 
                         get_api_calls_remaining,
                         get_summary_from_raw, print_summary)

class FaceProcessingTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """Run once before all tests in this test class."""
        cls.pic_data = load_raw_output(
            "fixtures/44454831-0aa7-4c84-be81-2874f4b3c3bc.json")
        
        filename = 'fixtures/scraperwiki_followers.json'
        with open(filename, 'rb') as f:
            cls.twitter_data = json.load(f)

        cls.twitter_raw_data = load_raw_output(
            "fixtures/eaaed005-c817-4405-b2cd-74adc638063b.json")

        cls.twitter_enrichment_data = get_summary_from_raw(cls.twitter_raw_data)

    def test_load_raw_output(self):
        assert_equal(14, len(self.pic_data))

    def test_summarise_for_valid_pic_data(self):
        summary = summarise_face_info(self.pic_data[0])

        expected_summary = [OrderedDict(
                           {u'name': u'Pete', 
                            u'age': 14.25,
                            u'gender': u'female',
                            u'race': u'white',
                            u'emotion': u'happy',
                            u'has_glasses': 'no',
                            u'target_number': 1,
                            u'total_targets': 1})]

        assert_equal(expected_summary, summary)

    #def tests_get_twitter_data_by_api(self):
    #    pic_data = get_twitter_data_by_api(number=10)
    #    assert_equal(10, len(pic_data))

    def test_calls_remaining(self):
        calls_remaining = get_api_calls_remaining()
        print("\nAPI calls remaining: \t {}".format(calls_remaining))
        assert_equal(int, type(calls_remaining))

    def test_finds_multiple_faces(self):
        for entry in self.twitter_raw_data:
            name = entry.keys()[0]
            data = entry[entry.keys()[0]]
            print name, len(data['face_detection'])

    def test_prints_output(self):
        summary_output = get_summary_from_raw(self.pic_data)
        print_summary(summary_output)
